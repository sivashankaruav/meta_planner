﻿using System;
namespace MissionPlanner
{
    partial class MainV2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            Console.WriteLine("mainv2_Dispose");
            if (PluginThreadrunner != null)
                PluginThreadrunner.Dispose();
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainV2));
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.CTX_mainmenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.autoHideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fullScreenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readonlyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.connectionOptionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFlightData = new System.Windows.Forms.ToolStripButton();
            this.MenuFlightPlanner = new System.Windows.Forms.ToolStripButton();
            this.Setup = new System.Windows.Forms.ToolStripMenuItem();
            this.InitialSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ConfigTuneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SimToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TermToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StartButton = new System.Windows.Forms.ToolStripButton();
            this.PauseButton = new System.Windows.Forms.ToolStripButton();
            this.StopButton = new System.Windows.Forms.ToolStripButton();
            this.ResumeButton = new System.Windows.Forms.ToolStripButton();
            this.DrawPoly = new System.Windows.Forms.ToolStripButton();
            this.SG = new System.Windows.Forms.ToolStripButton();
            this.MenuConnect = new System.Windows.Forms.ToolStripButton();
            this.toolStripConnectionControl = new MissionPlanner.Controls.ToolStripConnectionControl();
            this.menu = new MissionPlanner.Controls.MyButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.MainMenu.SuspendLayout();
            this.CTX_mainmenu.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainMenu
            // 
            this.MainMenu.BackgroundImage = global::MissionPlanner.Properties.Resources.bgdark;
            this.MainMenu.ContextMenuStrip = this.CTX_mainmenu;
            this.MainMenu.GripMargin = new System.Windows.Forms.Padding(0);
            this.MainMenu.ImageScalingSize = new System.Drawing.Size(0, 0);
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuFlightData,
            this.MenuFlightPlanner,
            this.Setup,
            this.StartButton,
            this.PauseButton,
            this.StopButton,
            this.ResumeButton,
            this.DrawPoly,
            this.SG,
            this.MenuConnect,
            this.toolStripConnectionControl});
            resources.ApplyResources(this.MainMenu, "MainMenu");
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Stretch = false;
            this.MainMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.MainMenu_ItemClicked);
            this.MainMenu.MouseLeave += new System.EventHandler(this.MainMenu_MouseLeave);
            // 
            // CTX_mainmenu
            // 
            this.CTX_mainmenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.CTX_mainmenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.autoHideToolStripMenuItem,
            this.fullScreenToolStripMenuItem,
            this.readonlyToolStripMenuItem,
            this.connectionOptionsToolStripMenuItem});
            this.CTX_mainmenu.Name = "CTX_mainmenu";
            resources.ApplyResources(this.CTX_mainmenu, "CTX_mainmenu");
            // 
            // autoHideToolStripMenuItem
            // 
            this.autoHideToolStripMenuItem.CheckOnClick = true;
            this.autoHideToolStripMenuItem.Name = "autoHideToolStripMenuItem";
            resources.ApplyResources(this.autoHideToolStripMenuItem, "autoHideToolStripMenuItem");
            this.autoHideToolStripMenuItem.Click += new System.EventHandler(this.autoHideToolStripMenuItem_Click);
            // 
            // fullScreenToolStripMenuItem
            // 
            this.fullScreenToolStripMenuItem.CheckOnClick = true;
            this.fullScreenToolStripMenuItem.Name = "fullScreenToolStripMenuItem";
            resources.ApplyResources(this.fullScreenToolStripMenuItem, "fullScreenToolStripMenuItem");
            this.fullScreenToolStripMenuItem.Click += new System.EventHandler(this.fullScreenToolStripMenuItem_Click);
            // 
            // readonlyToolStripMenuItem
            // 
            this.readonlyToolStripMenuItem.CheckOnClick = true;
            this.readonlyToolStripMenuItem.Name = "readonlyToolStripMenuItem";
            resources.ApplyResources(this.readonlyToolStripMenuItem, "readonlyToolStripMenuItem");
            this.readonlyToolStripMenuItem.Click += new System.EventHandler(this.readonlyToolStripMenuItem_Click);
            // 
            // connectionOptionsToolStripMenuItem
            // 
            this.connectionOptionsToolStripMenuItem.Name = "connectionOptionsToolStripMenuItem";
            resources.ApplyResources(this.connectionOptionsToolStripMenuItem, "connectionOptionsToolStripMenuItem");
            this.connectionOptionsToolStripMenuItem.Click += new System.EventHandler(this.connectionOptionsToolStripMenuItem_Click);
            // 
            // MenuFlightData
            // 
            resources.ApplyResources(this.MenuFlightData, "MenuFlightData");
            this.MenuFlightData.ForeColor = System.Drawing.Color.SteelBlue;
            this.MenuFlightData.Margin = new System.Windows.Forms.Padding(0);
            this.MenuFlightData.Name = "MenuFlightData";
            this.MenuFlightData.Click += new System.EventHandler(this.MenuFlightData_Click);
            // 
            // MenuFlightPlanner
            // 
            resources.ApplyResources(this.MenuFlightPlanner, "MenuFlightPlanner");
            this.MenuFlightPlanner.ForeColor = System.Drawing.Color.SteelBlue;
            this.MenuFlightPlanner.Margin = new System.Windows.Forms.Padding(0);
            this.MenuFlightPlanner.Name = "MenuFlightPlanner";
            this.MenuFlightPlanner.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.MenuFlightPlanner.Click += new System.EventHandler(this.MenuFlightPlanner_Click);
            // 
            // Setup
            // 
            resources.ApplyResources(this.Setup, "Setup");
            this.Setup.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.InitialSetupToolStripMenuItem,
            this.ConfigTuneToolStripMenuItem,
            this.SimToolStripMenuItem,
            this.TermToolStripMenuItem});
            this.Setup.ForeColor = System.Drawing.Color.SteelBlue;
            this.Setup.Name = "Setup";
            // 
            // InitialSetupToolStripMenuItem
            // 
            this.InitialSetupToolStripMenuItem.ForeColor = System.Drawing.Color.DeepSkyBlue;
            resources.ApplyResources(this.InitialSetupToolStripMenuItem, "InitialSetupToolStripMenuItem");
            this.InitialSetupToolStripMenuItem.Name = "InitialSetupToolStripMenuItem";
            this.InitialSetupToolStripMenuItem.Click += new System.EventHandler(this.InitialSetupToolStripMenuItem_Click);
            // 
            // ConfigTuneToolStripMenuItem
            // 
            this.ConfigTuneToolStripMenuItem.ForeColor = System.Drawing.Color.DeepSkyBlue;
            resources.ApplyResources(this.ConfigTuneToolStripMenuItem, "ConfigTuneToolStripMenuItem");
            this.ConfigTuneToolStripMenuItem.Name = "ConfigTuneToolStripMenuItem";
            this.ConfigTuneToolStripMenuItem.Click += new System.EventHandler(this.ConfigTuneToolStripMenuItem_Click);
            // 
            // SimToolStripMenuItem
            // 
            this.SimToolStripMenuItem.ForeColor = System.Drawing.Color.DeepSkyBlue;
            resources.ApplyResources(this.SimToolStripMenuItem, "SimToolStripMenuItem");
            this.SimToolStripMenuItem.Name = "SimToolStripMenuItem";
            this.SimToolStripMenuItem.Click += new System.EventHandler(this.SimToolStripMenuItem_Click);
            // 
            // TermToolStripMenuItem
            // 
            this.TermToolStripMenuItem.ForeColor = System.Drawing.Color.DeepSkyBlue;
            resources.ApplyResources(this.TermToolStripMenuItem, "TermToolStripMenuItem");
            this.TermToolStripMenuItem.Name = "TermToolStripMenuItem";
            this.TermToolStripMenuItem.Click += new System.EventHandler(this.TermToolStripMenuItem_Click);
            // 
            // StartButton
            // 
            resources.ApplyResources(this.StartButton, "StartButton");
            this.StartButton.ForeColor = System.Drawing.Color.SteelBlue;
            this.StartButton.Margin = new System.Windows.Forms.Padding(0);
            this.StartButton.Name = "StartButton";
            this.StartButton.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // PauseButton
            // 
            resources.ApplyResources(this.PauseButton, "PauseButton");
            this.PauseButton.ForeColor = System.Drawing.Color.SteelBlue;
            this.PauseButton.Margin = new System.Windows.Forms.Padding(0);
            this.PauseButton.Name = "PauseButton";
            this.PauseButton.Click += new System.EventHandler(this.PauseButton_Click);
            // 
            // StopButton
            // 
            resources.ApplyResources(this.StopButton, "StopButton");
            this.StopButton.ForeColor = System.Drawing.Color.SteelBlue;
            this.StopButton.Margin = new System.Windows.Forms.Padding(0);
            this.StopButton.Name = "StopButton";
            this.StopButton.Click += new System.EventHandler(this.StopButton_Click);
            // 
            // ResumeButton
            // 
            resources.ApplyResources(this.ResumeButton, "ResumeButton");
            this.ResumeButton.ForeColor = System.Drawing.Color.SteelBlue;
            this.ResumeButton.Margin = new System.Windows.Forms.Padding(0);
            this.ResumeButton.Name = "ResumeButton";
            this.ResumeButton.Click += new System.EventHandler(this.ResumeButton_Click_1);
            // 
            // DrawPoly
            // 
            resources.ApplyResources(this.DrawPoly, "DrawPoly");
            this.DrawPoly.ForeColor = System.Drawing.Color.SteelBlue;
            this.DrawPoly.Margin = new System.Windows.Forms.Padding(0);
            this.DrawPoly.Name = "DrawPoly";
            this.DrawPoly.Click += new System.EventHandler(this.DrawPoly_Click);
            // 
            // SG
            // 
            resources.ApplyResources(this.SG, "SG");
            this.SG.ForeColor = System.Drawing.Color.SteelBlue;
            this.SG.Margin = new System.Windows.Forms.Padding(0);
            this.SG.Name = "SG";
            // 
            // MenuConnect
            // 
            this.MenuConnect.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.MenuConnect.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.MenuConnect, "MenuConnect");
            this.MenuConnect.ForeColor = System.Drawing.Color.SteelBlue;
            this.MenuConnect.Margin = new System.Windows.Forms.Padding(0);
            this.MenuConnect.Name = "MenuConnect";
            this.MenuConnect.Click += new System.EventHandler(this.MenuConnect_Click);
            // 
            // toolStripConnectionControl
            // 
            this.toolStripConnectionControl.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripConnectionControl.BackgroundImage = global::MissionPlanner.Properties.Resources.bgdark;
            this.toolStripConnectionControl.ForeColor = System.Drawing.Color.Black;
            this.toolStripConnectionControl.Margin = new System.Windows.Forms.Padding(0);
            this.toolStripConnectionControl.Name = "toolStripConnectionControl";
            resources.ApplyResources(this.toolStripConnectionControl, "toolStripConnectionControl");
            this.toolStripConnectionControl.MouseLeave += new System.EventHandler(this.MainMenu_MouseLeave);
            // 
            // menu
            // 
            resources.ApplyResources(this.menu, "menu");
            this.menu.Name = "menu";
            this.menu.UseVisualStyleBackColor = true;
            this.menu.MouseEnter += new System.EventHandler(this.menu_MouseEnter);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.MainMenu);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            this.panel1.MouseLeave += new System.EventHandler(this.MainMenu_MouseLeave);
            // 
            // MainV2
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menu);
            this.KeyPreview = true;
            this.MainMenuStrip = this.MainMenu;
            this.Name = "MainV2";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainV2_KeyDown);
            this.Resize += new System.EventHandler(this.MainV2_Resize);
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.CTX_mainmenu.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripButton MenuFlightData;
        private System.Windows.Forms.ToolStripButton MenuFlightPlanner;
        public System.Windows.Forms.ToolStripButton MenuConnect;
        private Controls.ToolStripConnectionControl toolStripConnectionControl;
        private Controls.MyButton menu;
        public System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ContextMenuStrip CTX_mainmenu;
        private System.Windows.Forms.ToolStripMenuItem autoHideToolStripMenuItem;
        public System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem fullScreenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readonlyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem connectionOptionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Setup;
        private System.Windows.Forms.ToolStripButton DrawPoly;
        private System.Windows.Forms.ToolStripButton SG;
        private System.Windows.Forms.ToolStripMenuItem InitialSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ConfigTuneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SimToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TermToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton StopButton;
        private System.Windows.Forms.ToolStripButton StartButton;
        private System.Windows.Forms.ToolStripButton PauseButton;
        private System.Windows.Forms.ToolStripButton ResumeButton;
    }
}